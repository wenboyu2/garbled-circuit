import pickle
import random
from circuit import *
from utility import *
import zmq
import time

RSA_bits = 512
ranintLow = 0
alice_transmit_1 = "alice_transmit_1.data"
bob_transmit_1 = "bob_transmit_1.data"
alice_transmit_2 = "alice_transmit_2.data"


class Bob:
    def __init__(self, bs):
        self.bip = []
        self.gc = None
        self.alice_input = None
        self.all_inputs = []
        context = zmq.Context()
        self.socket = context.socket(zmq.REQ)
        self.socket.connect('tcp://127.0.0.1:5556')
        self.socket.send('bob:ready')
        self.recv_msg = None
        self.bs = bs

    def start(self):
        input_counter = 0
        while True:
            time.sleep(0.5)
            self.recv_msg = self.socket.recv()
            print 'message received: ', self.recv_msg
            if self.recv_msg == 'alice:ready':
                self.socket.send('bob:request circuit')
            elif self.recv_msg == 'alice:circuit built':
                self.load_circuit()
                self.socket.send('bob:request input')
            elif self.recv_msg == 'alice:give_input':
                self.ask_input(self.bs[input_counter])
                self.socket.send('bob:ask_input')
            elif self.recv_msg == 'alice:transfer':
                self.receive()
                input_counter += 1
                self.socket.send('bob:receive')
            elif self.recv_msg == 'alice:do_circuit':
                self.run_circuit()
                self.socket.send('bob:done')
                break
            else:
                self.socket.send('bob?')

    def ask_input(self, b):
        self.b = b
        alice = read_json(alice_transmit_1)
        self.k = random.randint(ranintLow, alice["n"])
        g = alice["xs"][b] % alice["n"]
        h = pow(self.k, alice["e"], alice["n"])
        self.v = (g + h) % alice["n"]
        j = {
            "v": self.v
        }
        write_json(bob_transmit_1, j)
        print("Now please run Alice.transfer()")

    def receive(self):
        alice = read_json(alice_transmit_2)
        message_not = alice["ms_not"][self.b]
        message = int(message_not - self.k)
        print("Bob has the message as", message)
        self.bip.append(int(message))
        return message

    def load_circuit(self):
        self.gc = pickle.load(open("gc.p", "rb"))
        self.alice_input = pickle.load(open("alice_input.p", "rb"))

    def run_circuit(self):
        bidx = 0
        idx = 0
        for aip in self.alice_input:
            if aip is not None:
                self.all_inputs.append(aip)
            else:
                self.all_inputs.append(self.bip[bidx])
                bidx += 1
            idx += 1
        print 'all input values', self.all_inputs
        for i in range(len(self.all_inputs)):
            self.gc.set_input_value(self.all_inputs[i], i)

        self.gc.evaluate(self.gc.root)

if __name__ == '__main__':
    bs = [0, 1]
    bob = Bob(bs)
    bob.start()
