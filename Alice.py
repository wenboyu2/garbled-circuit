import math
import pickle
import rsa
import random
from circuit import *
from utility import *
import zmq
import time

RSA_bits = 512
ranintLow = 0
alice_transmit_1 = "alice_transmit_1.data"
bob_transmit_1 = "bob_transmit_1.data"
alice_transmit_2 = "alice_transmit_2.data"


class Alice:
    def __init__(self):
        context = zmq.Context()
        self.socket = context.socket(zmq.REP)
        self.socket.bind('tcp://127.0.0.1:5556')
        self.recv_msg = None

    def start(self):
        input_counter = 0
        while True:
            time.sleep(0.5)
            self.recv_msg = self.socket.recv()
            print 'message received: ', self.recv_msg
            if self.recv_msg == 'bob:ready':
                self.socket.send('alice:ready')
                input_counter = 0
            elif self.recv_msg == 'bob:request circuit':
                self.build_circuit()
                self.socket.send('alice:circuit built')

            elif self.recv_msg in ['bob:request input', 'bob:receive'] and input_counter < len(self.bob_input_pairs):
                self.give_input(self.bob_input_pairs[input_counter])
                self.socket.send('alice:give_input')

            elif input_counter == len(self.bob_input_pairs):
                self.socket.send('alice:do_circuit')

            elif self.recv_msg == 'bob:ask_input':
                self.transfer()
                input_counter += 1
                self.socket.send('alice:transfer')
            elif self.recv_msg == 'bob:done':
                break
            else:
                self.socket.send('ALICE!!')


    def give_input(self, M):
        self.M = M
        (pubkey, privkey) = rsa.newkeys(RSA_bits)
        self.pubkey = pubkey
        self.privkey = privkey

        self.x_0 = random.randint(ranintLow, pubkey.n)
        self.x_1 = random.randint(ranintLow, pubkey.n)
        j = {
            "n": self.pubkey.n,
            "e": self.pubkey.e,
            "xs": [self.x_0, self.x_1],
        }
        write_json(alice_transmit_1, j)
        print("Now please initiate Bob")

    def transfer(self):
        bob = read_json(bob_transmit_1)
        v = bob["v"]
        k_0 = pow((v - self.x_0), self.privkey.d, self.pubkey.n)
        k_1 = pow((v - self.x_1), self.privkey.d, self.pubkey.n)
        #print("Alice have k as: ", k_0, " ", k_1)
        m_0_not = self.M[0] + k_0
        m_1_not = self.M[1] + k_1
        j = {
            "ms_not" : [m_0_not,m_1_not]
        }
        write_json(alice_transmit_2, j)
        #print("m_0_not and m_1_not are generate into file: ", alice_transmit_2)
        print("Now please run Bob.receive() to reveice ")

    def input_factory(self):
        for input_pairs in self.bob_input_pairs:
            yield input_pairs

    def build_circuit(self):

        # Create normal AND gate
        ng0 = NormalGate('and', None, None, 'g0')
        # Garbled the AND gate
        gg0 = GarbledGate(ng0)

        ng1 = NormalGate('and', None, None, 'g1')
        gg1 = GarbledGate(ng1)

        ng2 = NormalGate('or', ng0, ng1, 'g2', is_output=True)
        gg2 = GarbledGate(ng2)
        # Set this gate to be output gate
        # ng3 = NormalGate('not', ng2, None, 'g3', is_output=True)
        # gg3 = GarbledGate(ng3)

        ggs = [gg0, gg1, gg2]

        # Initialize inputs
        in0 = Input('i0')
        in1 = Input('i1')
        in2 = Input('i2')
        in3 = Input('i3')


        inputs = [in0, in1, in2, in3]

        # Build garbled circuit and set the output gate to be the third gate
        gc = GarbledCircuit(ggs, 2)
        gc.add_inputs(inputs)

        # Add wire from the first input to the first gate
        gc.add_wire(0, 0, is_from_input=True)
        gc.add_wire(1, 0, is_from_input=True)

        gc.add_wire(2, 1, is_from_input=True)
        gc.add_wire(3, 1, is_from_input=True)
        # Add wire from the first gate to the third gate
        gc.add_wire(0, 2)
        gc.add_wire(1, 2)

        all_input_pairs = [gg0.keys[0], gg0.keys[1], gg1.keys[0], gg1.keys[1]]
        self.bob_input_pairs = [gg0.keys[1], gg1.keys[1]]
        # Export the garbled circuit
        pickle.dump(gc, open("gc.p", "wb"))
        # Export Alice's inputs
        pickle.dump([gg0.keys[0][0], None, gg1.keys[0][0], None], open("alice_input.p", "wb"))

if __name__ == '__main__':
    alice = Alice()
    alice.start()
