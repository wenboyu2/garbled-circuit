import json


def write_json(file_name, j):
    with open(file_name, 'w') as outfile:
        json.dump(j, outfile, separators=(',', ':'))


def read_json(file_name):
    with open(file_name) as data_file:
        j = json.load(data_file)
    return j
