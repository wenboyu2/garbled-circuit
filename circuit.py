import copy
import random
import pprint
from Crypto.Cipher import AES
from Crypto import Random

pp = pprint.PrettyPrinter(indent=4)

def generate_random_16x_string():
    string = '%f' % (random.random()*10**18)
    return string.split('.')[0][:16]

class NormalGate(object):

    """docstring for Gate"""
    def __init__(self, gate_type, gate0, gate1, name, is_output=False):
        super(NormalGate, self).__init__()
        self.gate_type = gate_type
        self.truth_table = self.generate_truth_table(gate_type)
        self.is_output = is_output
        self.name = name
        self.keys = self.generate_keys(gate0, gate1)

    def generate_truth_table(self, gate_type):
        if gate_type == 'and':
            return [
                [0, 0, 0],
                [0, 1, 0],
                [1, 0, 0],
                [1, 1, 1],
            ]
        elif gate_type == 'or':
            return [
                [0, 0, 0],
                [0, 1, 1],
                [1, 0, 1],
                [1, 1, 1],
            ]
        elif gate_type == 'xor':
            return [
                [0, 0, 0],
                [0, 1, 1],
                [1, 0, 1],
                [1, 1, 0],
            ]

    def generate_keys(self, gate0, gate1):
        fresh_keys = map(lambda x: [self.convert_to_int('%f' % (random.random()*10**18)), self.convert_to_int('%f' % (random.random()*10**18))], range(len(self.truth_table[0])))
        if gate0:
            fresh_keys[0] = gate0.keys[2]
        if gate1:
            fresh_keys[1] = gate1.keys[2]
        return fresh_keys

    def convert_to_int(self, string):
        string = string.split('.')[0][:16]
        return int(string)

class GarbledGate(object):
    """docstring for GarbledGate"""
    def __init__(self, normal_gate):
        super(GarbledGate, self).__init__()
        self.name = normal_gate.name
        self.id = generate_random_16x_string()
        self.is_output = normal_gate.is_output
        self.gate_type = normal_gate.gate_type
        self.truth_table = self.generate_truth_table(normal_gate)
        print pp.pprint(self.truth_table)
        self.left = None
        self.right = None
        self.keys = normal_gate.keys
        self.val = None

    def generate_truth_table(self, normal_gate):
        normal_table = normal_gate.truth_table
        keys = normal_gate.keys
        garbled_table = copy.deepcopy(normal_table)

        range_num = 3
        if normal_gate.is_output:
            range_num -= 1
        if self.gate_type == 'not':
            range_num -= 1

        for row in garbled_table:
            for i in range(range_num):
                row[i] = keys[i][row[i]]
            if self.is_output:
                continue
            row[2] = self.encrypt(str(row[0]), str(row[1]), str(row[2]))

        print pp.pprint(garbled_table)
        random.shuffle(garbled_table)
        return garbled_table

    def encrypt(self, kx, ky, kw):
        c1 = AES.new(kx, AES.MODE_CBC, self.id)
        c2 = AES.new(ky, AES.MODE_CBC, self.id)
        m1 = c1.encrypt(kw)
        m2 = c2.encrypt(m1)
        return m2

    def add_left(self, obj):
        self.left = obj

    def add_right(self, obj):
        self.right = obj

    def decrypt(self, left, right, kw):
        c1 = AES.new(str(left), AES.MODE_CBC, self.id)
        c2 = AES.new(str(right), AES.MODE_CBC, self.id)
        m2 = c2.decrypt(str(kw))
        m = int(c1.decrypt(m2))
        return m

    def get_val(self, left, right):
        for row in self.truth_table:
            if left.val == row[0] and right.val == row[1]:
                if self.is_output:
                    return row[2]
                self.val = self.decrypt(left.val, right.val, row[2])
                return self.val
class Input(object):
    """docstring for Input"""
    def __init__(self, name):
        super(Input, self).__init__()
        self.val = None
        self.name = name
        self.left, self.right = None, None

    def get_val(self, lval, rval):
        return self.val


class GarbledCircuit(object):
    """docstring for GarbledCircuit"""
    def __init__(self, ggs, output_gate_num):
        super(GarbledCircuit, self).__init__()
        self.ggs = ggs
        self.inputs = None
        self.root = self.ggs[output_gate_num]

    def add_inputs(self, inputs):
        self.inputs = inputs

    def set_input_value(self, value, idx):
        self.inputs[idx].val = value

    def add_wire(self, f, t, is_from_input=False, is_left=True):
        from_gate = self.inputs[f] if is_from_input else self.ggs[f]
        to_gate = self.ggs[t]

        if is_left and to_gate.left is None:
            to_gate.add_left(from_gate)
        else:
            to_gate.add_right(from_gate)

    def evaluate(self, node):
        return self.traverse(node)

    def traverse(self, root):
        if root:
            self.traverse(root.left)
            self.traverse(root.right)
            curr_val = root.get_val(root.left, root.right)
            print root.name, curr_val
            return curr_val
